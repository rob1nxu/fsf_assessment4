#!/bin/bash
ASSESSMENT3_HOME=$HOME/assessment4rx

if [ -d "$ASSESSMENT4_HOME" ]; then
   zip -r $HOME/backup/$(date +"%m-%d-%Y").zip $ASSESSMENT4_HOME/
fi
rm -rf $ASSESSMENT4_HOME/*
rm -rf $ASSESSMENT4_HOME/.git