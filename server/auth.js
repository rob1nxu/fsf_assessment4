"use strict";

var LocalStrategy = require("passport-local").Strategy;

var FacebookStrategy = require("passport-facebook").Strategy;

var bcrypt = require("bcryptjs");

var config = require("./config");

var Users = require("./database").Users;


module.exports = function(database, passport){ //define strategy below

// step2
    function authenticate(username, password, done){
        console.log("AUTHENTICATE the user ====================");
        console.log("username: " + username);
        console.log("password: " + password);
        console.log("done " + done);

       // done(null, "ken@ken.com");

        Users.findOne({
            where: {
                username: username
            }
        }).then(function(result) {
            console.log("result is: " + JSON.stringify(result));
            if (!result) {
                return done(null, false);
            } 
            else {
                if (bcrypt.compareSync(password, result.password)) {
                    console.log("COMPARING PASSWORDS HERE ==============");
                    console.log("password: " + password);
                    console.log("result.password: " + result.password);
                    //code below unclear
                    var whereClause = {};
                    whereClause.username = username;
                    console.log(" whereClause.username: "  + username);
                    Users
                        .update({ reset_password_token: null }, { where: whereClause });
                    console.log("result is " + JSON.stringify(result));

                    return done(null, result);
                } else {
                    return done(null, false);
                }
           }
        }).catch(function(err) {
            return done(err, false);
        });
    }

    //step 1
    passport.use(new LocalStrategy({
        usernameField:"username",
        passwordField:"password"
    }, authenticate));

    passport.serializeUser(function(user, done){
        console.log("SERALIZE");
        console.log("username: " + JSON.stringify(user));
        // console.log("password" + password);
        console.log("done " + done);
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        console.log("DE--SEREALIZE");
        console.log("user: " + JSON.stringify(user));

        Users.findOne({
            where: {
                username: user.username
            }
        }).then(function(result) {
        console.log("result: " + JSON.stringify(result));
        console.log("BREAK ===================");
            if (result) {
                done(null, user);
            }
        }).catch(function(err) {
            done(null, false);
        });
    });

};



