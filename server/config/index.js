"use strict";

var ENV = process.env.NODE_ENV || "development";

module.exports = require("./" + ENV + ".js") || {}; //turns this file into a js object for another js script to linkup with