
var express = require("express");

var bodyParser = require("body-parser");

var cookieParser = require("cookie-parser");


var session = require("express-session");
var passport = require("passport");
var config = require("./config"); //links up 
const NODE_PORT = config.port;


var app = express();


app.use(cookieParser()); 


app.use(bodyParser.urlencoded({ limit: "50mb", extended: true}));

app.use(bodyParser.json({limit: "50mb"}));

app.use(session({

    secret:"4e2c7bdd6f0042fece29b19bc6bed6edddd9a91f58c1c1d86b3368517acfd2fa", //secret key here
    resave: false,
    saveUninitialized: true

}));

app.use(passport.initialize()); // initialize passport in express. 
app.use(passport.session()); // linkup passport with session


var database = require("./database");
require("./auth")(database, passport); //
require("./routes")(app, database, passport); //we are passing (app, database, passport) into routes



//require("./routes")(app); //this is linking up the routesjs into express. or passing the routesjs into express

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at " + config.domain_name); //not hardcoding but obtaining the domainname and nodeport based on configjs
});
