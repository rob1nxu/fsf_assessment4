(function () {

    angular.module("AnvApp")
        .service("AuthService", [
            "$http",
            "$q","$state",
            AuthService
        ]);

    function AuthService($http, $q, $state) {
        // var self = this;
      var  authservice = this;
        var user = null;
        //authservice.authResult = "";

        // authservice.Auser = "";
        console.log("authservice.Auser = " + authservice.Auser);


         return ({
                isUserLoggedIn: isUserLoggedIn,
               // getUserStatus: getUserStatus,
                login: login,
                logout: logout,
                register: register,
               // resetPassword: resetPassword,
               // changePassword: changePassword
            });



        function register(username, email, password, fullname, contact) {
            console.log("username:%s ", username);
            console.log("email:%s ", email);

            // create a new instance of deferred
            var deferred = $q.defer();
            // send a post request to the server
            $http.post('/register',
                { username: username, email: email, password: password, fullname: fullname, contact: contact })
                // handle success
                .then(function (data) {
                    console.log("data is: $s", data);
                    var status = data.status;

                    console.log("status is: $s", status);
                    if (status) {
                        console.log("signup successful")
                        // res.send.json(success);
                        deferred.resolve();
                    } else {
                        console.log("signup unsuccessful")
                        deferred.reject();
                    }
                })
                // handle error
                .catch(function (data) {
                    // Flash.clear();
                    // Flash.create('danger', "Ooops something went wrong!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }



     
         function login (userProfile){
            console.log(userProfile);
            var deferred = $q.defer();
            $http.post("/security/login", userProfile)
            .then(function(data){
                console.log(data);
                if(data.status == 200){
                 
                    deferred.resolve();
                }
            }).catch(function(error){
                console.log(error);
                user = false;
                deferred.reject();
            })
            return deferred.promise;
        }




            function isUserLoggedIn (cb){      //here
             $http.get("/status/user")
            //return (works when return is removed)
            // .then(function(data){
            //     console.log(data);
            //     user = true;
            //     // cb(user);


            // }).catch(function(error){
            //     console.log(error);
            //     user = false;
            //     // cb(user);
            // })

              .then(function (data) {
                        console.log("data is: " + JSON.stringify(data));
                        authResult = JSON.stringify(data);
                        console.log("what is authResult: " + authResult);
                        // console.log("username logged in is: %s", data.data.username);
                        authservice.data = data;
                      //  authservice.authUsername = data.data.username;
                     authservice.authUsername = data.data.username;

                        console.log("authservice.authUsername is: " + authservice.authUsername);

                        if(data["data"] != ''){
                          //  user = true;
                            user = data.data.username;

                            // return authservice;
                        authservice.authUsername = data.data.username;
                        console.log("authservice.authUsername is: " + authservice.authUsername);
                        
                            cb(user);               //do this first then rreturn to
                        } else {
                            user = false;
                            cb(user);
                        }
                    });
        }


        // function isUserLoggedIn() {
        //     if (user) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }





     
            function logout() {

                // create a new instance of deferred
                var deferred = $q.defer();

                // send a get request to the server
                $http.get('/logout')
                // handle success
                    .then(function (data) {
                        user = false;
                        deferred.resolve();
                    })
                    // handle error
                    .catch(function (data) {
                        user = false;
                        deferred.reject();
                    });

                // return promise object
                return deferred.promise;

            }

    }

})();