(function () {

    angular.module("AnvApp")
           .service("ProfileService", ["$http","$q",ProfileService]);

    function ProfileService($http, $q) {
        // var self = this;
        var profileservice = this;

        // var queryProfile = profileservice.getProfile;

        // function getUserProfile(queryProfile){


        // }
       
        //  function getUserProfile(queryProfile){    *********** //take note writing this way will not work. *********
        //     return $http({
        //         method: 'GET'
        //         , url: 'api/users/userprofile'
        //         , params: {
        //             "queryProfile": queryProfile
        //         }
        //     });
        // }

        profileservice.getUserProfile = function(queryProfile){
            return $http({
                method: 'GET'
                , url: 'api/users/userprofile'
                , params: {
                    "queryProfile": queryProfile
                }
            });
        }


    }
})();