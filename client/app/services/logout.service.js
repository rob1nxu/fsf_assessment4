(function () {

    angular.module("AnvApp")
        .service("LogoutSvc", [
            "$http",
            "$q","$state","AuthService",
            LogoutSvc
        ]);

    function LogoutSvc($http, $q, $state, AuthService) {
        var self = this;
        // var user = null;

    

     self.logout = function(){

            AuthService.logout()
            .then(function(){
                $state.go("login");
            
            }).catch(function(){
                console.error("Error !");

            });

        }
    } 

})();