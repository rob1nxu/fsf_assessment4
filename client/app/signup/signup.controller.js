
(function () {
    angular
        .module("AnvApp")
        .controller("SignupCtrl", ["$sanitize","$state","AuthService", SignupCtrl]);

    function SignupCtrl($sanitize, $state, AuthService){
        var signupctrl = this;
        signupctrl.username = "";
        signupctrl.email = "";
        signupctrl.password = "";
        signupctrl.confirmpassword = "";
        signupctrl.fullname = "";
        signupctrl.contact = "";

        signupctrl.showDetails = false;

         signupctrl.signup = function () {
            AuthService.register($sanitize(signupctrl.username), $sanitize(signupctrl.email), $sanitize(signupctrl.password), $sanitize(signupctrl.fullname),$sanitize(signupctrl.contact) )
                .then(function () {
                   // vm.disabled = false;
                  // console.log("result at AuthService.register: $s", result);
                        signupctrl.username = "";
                        signupctrl.email = "";
                        signupctrl.password = "";
                        signupctrl.confirmpassword = "";
                        signupctrl.fullname = "";
                        signupctrl.contact = "";
                        signupctrl.showDetails = true;
                      // signupctrl.showDetails = result;

                   // Flash.clear();
                   // Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    //$state.go("SignIn");

                }).catch(function () {
                console.error("registration having issues");
            });
        };

        // vm.progressbar = ngProgressFactory.createInstance();

        // vm.inputType = 'password';
        // vm.passwordCheckbox = false;

        // // Hide & show password function
        // vm.hideShowPassword = function(){
        //     if (vm.inputType == 'password')
        //         vm.inputType = 'text';
        //     else
        //         vm.inputType = 'password';
        // };

        // vm.login = function () {
        //     vm.progressbar.start();
        //     AuthFactory.login(vm.user)
        //         .then(function () {
        //             if(AuthFactory.isLoggedIn()){
        //                 vm.emailAddress = "";
        //                 vm.password = "";
        //                 vm.progressbar.complete();
        //                 $state.go("weddinggram");
        //             }else{
        //                 Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                 vm.progressbar.stop();
        //                 $state.go("SignIn");
        //             }
        //         }).catch(function () {
        //             vm.progressbar.stop();
        //             console.error("Error logging on !");
        //         });
        // };
    }
})();