
(function() {
    
    angular
        .module("AnvApp", [ 
            , "ui.router" 
            , "ngSanitize"
        ]);
})();
